import java.util.Date

/**
 * A demonstration program for debugging.
 * You should add a breakpoint to the line 14 (which prints Hello!) and hit Debug and after that observe
 * what is happening in the program. Step over and Step into are your commands of interest.
 * 
 * @author Danijel
 * @version 1.0
 */
public SampleProgram {

	public static void main(String[] args) {
	
		System.out.println("Hello!");
		System.out.println(new Date());
		
		String[] randomData = new String[]{"ana", "Ana", "1234321", "abba "};
		
		for (int i = 0; i < randomData.length; ++i) {
			boolean isPalindrome = isPalindrome(randomData[i]);
			String result = isPalindrome ? "is" : "is not";
			System.out.println("\"" + randomData[i] + "\" " + result + " palindrome.");
			
			int[] numberOfLettersAndDigits = countLettersAndDigits(randomData[i]);
			System.out.println("\"" + randomData[i] + "\" has " + stats[0] + " letters and " + stats[1] + "digits.");
			
		}
		
	}
	
	/**
	 * Counts letters and digits.
	 * 
	 * @param word a String to analyze
	 * @return number of letters and nummber of digits in an integer array
	 */
	public static int[] countLettersAndDigits(String word) {
		int[] stats = new int[2];
		for (int i = 0; i < word.length(); ++i) {
			char current = word.charAt(i);
			if (Character.isLetter(current)) {
				++stats[0];
			} else if (Character.isDigit(current)) {
				++stats[1];
			}
		}
		
		return stats;
	}
	
	/**
	 * Checks if the given letter is a palindrome or not.
	 * 
	 * @return true if the word is palindrome, false otherwise.
	 */
	public static boolean isPalindrome(String word) {
		for (int i = 0; i < word.length() / 2; ++i) {
			if (word.charAt(i) != word.charAt(word.length() - 1 - i)) {
				return false;
			}
		}
		
		return true;
	}
	
}
